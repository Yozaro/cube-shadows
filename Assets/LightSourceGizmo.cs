﻿using UnityEngine;

public class LightSourceGizmo : MonoBehaviour
{
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(transform.position, 0.06f);
	}
}
