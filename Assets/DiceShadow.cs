﻿using System;
using System.Collections.Generic;
using UnityEngine;

sealed public class DiceShadow : MonoBehaviour
{
	public Transform lightSource;
	public Material shadowMaterial;
	public float groundLevel;
	public Color shadowColor;
	public float scaleFactor = 1f;
	[Range(0f, 1f)]
	public float extrudeOffset;
	public float extrudeFactor = 0.1f;
	public bool debug;

	private Transform tm;
	private Vector3[] corners;

	private GameObject shadowGo;
	private Mesh mesh;
	private List<Vector3> vertices;
	private List<Color> colors;
	private List<int> triangles;

#if UNITY_EDITOR
	void OnValidate()
	{
		scaleFactor = Mathf.Max(scaleFactor, 0.1f);
		extrudeFactor = Mathf.Max(extrudeFactor, 0f);
	}
#endif

	void Start()
	{
		tm = transform;

		shadowGo = new GameObject("Shadow", typeof(MeshFilter), typeof(MeshRenderer));
		shadowGo.GetComponent<MeshRenderer>().sharedMaterial = shadowMaterial;

		createMesh();
		shadowGo.GetComponent<MeshFilter>().sharedMesh = mesh;
	}

	void OnDestroy()
	{
		Destroy(shadowGo);
		Destroy(mesh);
	}

	void LateUpdate()
	{
		refresh();
	}

	private void createMesh()
	{
		corners = new Vector3[]
		{
			new Vector3(-1, 1, 1),
			new Vector3(1, 1, 1),
			new Vector3(1, 1, -1),
			new Vector3(-1, 1, -1),
			new Vector3(-1, -1, 1),
			new Vector3(1, -1, 1),
			new Vector3(1, -1, -1),
			new Vector3(-1, -1, -1)
		};

		vertices = new List<Vector3>(24);
		colors = new List<Color>(24);
		triangles = new List<int>(24);

		mesh = new Mesh();

		refresh();
	}

	private Vector3[] edgeToLightNormals = new Vector3[8];
	private Vector3[] worldPoints = new Vector3[8];
	private void refresh()
	{
		vertices.Clear();
		colors.Clear();
		triangles.Clear();

		Matrix4x4 matrix = Matrix4x4.TRS(tm.position, tm.rotation, tm.localScale * scaleFactor * 0.5f);
		Vector3 light = lightSource.position;
		Vector3 center = new Vector3(0, 0, 0);

		for (int i = 0; i < 8; ++i)
		{
			worldPoints[i] = matrix.MultiplyPoint(corners[i]);

			Vector3 edge;
			Vector3 edgeToLight;
			calculateGroundPoint(worldPoints[i], light, out edge, out edgeToLight);

			vertices.Add(edge);
			edgeToLightNormals[i] = edgeToLight;
			center += edge;
		}

		center *= (1 / 8f);

		createConvexHull(light);
		createTriangles(center);

		for (int i = 0; i < vertices.Count; ++i)
			colors.Add(shadowColor);

		extrudeEdges(light);

		mesh.Clear();
		mesh.SetVertices(vertices);
		mesh.SetTriangles(triangles, 0);
		mesh.SetColors(colors);
	}

	private static readonly Vector3 GROUND_NORMAL = new Vector3(0, -1, 0);
	private void calculateGroundPoint(Vector3 world, Vector3 light, out Vector3 edge, out Vector3 edgeToLight)
	{
		Vector3 dir = new Vector3(world.x - light.x, world.y - light.y, world.z - light.z);
		dir = quickNormalize(dir);
		
		// http://www.habrador.com/tutorials/linear-algebra/4-plane-ray-intersection/
		Vector3 ground = new Vector3(0, groundLevel, 0);
		Vector3 worldToGround = new Vector3(ground.x - world.x, ground.y - world.y, ground.z - world.z);

		float denominator = Vector3.Dot(dir, GROUND_NORMAL);
		float dist = Vector3.Dot(worldToGround, GROUND_NORMAL) / denominator;

		edge = new Vector3(world.x + dir.x * dist, groundLevel, world.z + dir.z * dist);
		edgeToLight = new Vector3(-dir.x, -dir.y, -dir.z);
	}
	
	private Vector3[] hull = new Vector3[16];
	private Vector3[] tempVecs = new Vector3[8];
	private void createConvexHull(Vector3 light)
	{
		for (int i = 0; i < 8; ++i)
			tempVecs[i] = vertices[i];

		int hullSize;
		ConvexHull.Compute(tempVecs, ref hull, out hullSize);
		
		vertices.Clear();

		int len = hullSize;
		for (int i = 0; i < len; ++i)
		{
			Vector3 prev = hull[i > 0 ? i - 1 : len - 1];
			Vector3 current = hull[i];
			Vector3 next = hull[i < len - 1 ? i + 1 : 0];

			Vector3 lseg = new Vector3(
				current.x + (prev.x - current.x) * 0.05f,
				groundLevel,
				current.z + (prev.z - current.z) * 0.05f);
			Vector3 rseg = new Vector3(
				current.x + (next.x - current.x) * 0.05f,
				groundLevel,
				current.z + (next.z - current.z) * 0.05f);

			Vector3 mid = new Vector3(
				lseg.x + (rseg.x - lseg.x) * 0.5f,
				groundLevel,
				lseg.z + (rseg.z - lseg.z) * 0.5f);
			mid = new Vector3(
				mid.x + (current.x - mid.x) * 0.5f,
				groundLevel,
				mid.z + (current.z - mid.z) * 0.5f);

			vertices.Add(lseg);
			vertices.Add(mid);
			vertices.Add(rseg);
		}
	}

	private void createTriangles(Vector3 center)
	{
		vertices.Insert(0, center);

		int len = vertices.Count;
		for (int i = 1; i < len; ++i)
		{
			int next = (i < len - 1) ? i + 1 : 1;
			if (next >= len)
				throw new Exception();

			triangles.Add(0);
			triangles.Add(next);
			triangles.Add(i);
		}
	}

	private struct Extrude
	{
		public float size;
		public Vector3 edgeVec;
		public Vector3 normalVec;

		public int edgeIndex;
		public int pointIndex;
	}
	
	private Vector3 quickNormalize(Vector3 v)
	{
		// https://en.wikipedia.org/wiki/Fast_inverse_square_root
		float number = (v.x * v.x + v.y * v.y + v.z * v.z);
		const float threeHalfs = 1.5f;
		long i;
		float x2 = number * 0.5f;
		float sqrt = number;

		unsafe
		{
			i = *(long*) &sqrt;
			i = 0x5f3759df - (i >> 1);
			sqrt = *(float*) &i;
		}

		sqrt = sqrt * (threeHalfs - (x2 * sqrt * sqrt));

		return new Vector3(v.x * sqrt, v.y * sqrt, v.z * sqrt);
	}
	
	private List<Vector3> edgeToEdge = new List<Vector3>(24);
	private List<Extrude> extrudes = new List<Extrude>(24);
	private List<float> sizes = new List<float>(24);
	private void extrudeEdges(Vector3 light)
	{
		int vCount = vertices.Count;
		int lastIndex = vCount - 1;

		Vector3 center = vertices[0];
		Color extrudeColor = shadowColor;
		extrudeColor.a = 0f;

		edgeToEdge.Clear();
		extrudes.Clear();
		sizes.Clear();

		for (int i = 1; i < vCount; ++i)
		{
			Vector3 nextEdge = vertices[i < vCount - 1 ? i + 1 : 1];
			Vector3 vec = vertices[i];
			Vector3 diff = new Vector3(nextEdge.x - vec.x, 0, nextEdge.z - vec.z);
			diff = quickNormalize(diff);

			edgeToEdge.Add(diff);
		}

		for (int i = 1; i < vCount; ++i)
		{
			Vector3 edge = vertices[i];

			Vector3 left = edgeToEdge[(i > 1 ? i - 1 : vCount - 1) - 1];
			Vector3 right = edgeToEdge[(i < vCount - 1 ? i + 1 : 1) - 1];
			right = new Vector3(-right.x, 0, -right.z);

			Vector3 normal = quickNormalize(new Vector3(left.x + right.x, 0, left.z + right.z));

			//Vector3 toLight = edgeToLightNormals[(i - 1) / 3];
			//float normalDotLight = Vector3.Dot(normal, toLight);
			
			//Vector3 away = -toLight;
			//away.y = 0;
			//away = quickNormalize(away);
			//float bias = normalDotLight >= 0 ? 0 : -normalDotLight;
			//float extrudeSize = extrudeFactor * (bias + extrudeOffset);
			float extrudeSize = extrudeFactor * (1 + extrudeOffset);

			lastIndex++;

			extrudes.Add(new Extrude
			{
				size = extrudeSize,
				edgeVec = edge,
				normalVec = normal,
				edgeIndex = i,
				pointIndex = lastIndex
			});
			sizes.Add(extrudeSize);
		}
		
		int len = extrudes.Count;
		for (int i = 0; i < len; ++i)
		{
			Extrude prevExtrude = (i > 0) ? extrudes[i - 1] : extrudes[len - 1];
			Extrude nextExtrude = (i < len - 1) ? extrudes[i + 1] : extrudes[0];
			Extrude current = extrudes[i];

			float distToPrev = (prevExtrude.edgeVec - current.edgeVec).sqrMagnitude;
			float distToNext = (nextExtrude.edgeVec - current.edgeVec).sqrMagnitude;
			float prevBias = 1 - Mathf.Clamp(distToPrev / (distToPrev + distToNext), 0.2f, 0.8f);
			float nextBias = 1 - prevBias;

			if (prevBias >= 1f || nextBias >= 1f)
				throw new Exception();
			
			float prevSize = ((i > 0) ? sizes[i - 1] : sizes[len - 1]) * prevBias;
			float nextSize = ((i < len - 1) ? sizes[i + 1] : sizes[0]) * nextBias;
			
			current.size = (prevSize + nextSize) / 2f;

			if (current.size < prevSize && current.size < nextSize)
				throw new Exception();
			if (current.size > prevSize && current.size > nextSize)
				throw new Exception();

			extrudes[i] = current;
		}

		for (int i = 0; i < len; ++i)
		{
			Extrude extrude = extrudes[i];

			vertices.Add(new Vector3(
				extrude.edgeVec.x + extrude.normalVec.x * extrude.size,
				groundLevel,
				extrude.edgeVec.z + extrude.normalVec.z * extrude.size));
			colors.Add(extrudeColor);

			int edge = extrude.edgeIndex;
			int point = extrude.pointIndex;
			int nextEdge = edge < vCount - 1 ? edge + 1 : 1;
			int nextPoint = point < lastIndex ? point + 1 : vCount;

			triangles.Add(edge);
			triangles.Add(nextEdge);
			triangles.Add(point);

			triangles.Add(point);
			triangles.Add(nextEdge);
			triangles.Add(nextPoint);
		}
	}
}

public static class ConvexHull
{
	// https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
	public static void Compute(Vector3[] points, ref Vector3[] hull, out int hullSize)
	{
		hullSize = 0;

		int n = points.Length;
		if (n > 1)
		{
			int k = 0;

			for (int i = 0; i < 16; ++i)
				hull[i] = default(Vector3);

			Array.Sort(points, sort);

			for (int i = 0; i < n; ++i)
			{
				while (k >= 2 && cross(hull[k - 2], hull[k - 1], points[i]) <= 0)
					k--;
				hull[k++] = points[i];
			}

			for (int i = n - 2, t = k + 1; i >= 0; i--)
			{
				while (k >= t && cross(hull[k - 2], hull[k - 1], points[i]) <= 0)
					k--;
				hull[k++] = points[i];
			}

			hullSize = k - 1;
		}
	}

	private static int sort(Vector3 a, Vector3 b)
	{
		if (a.z > b.z)
			return 1;
		if (a.z == b.z)
			return 0;
		return -1;
	}

	private static float cross(Vector3 o, Vector3 a, Vector3 b)
	{
		return (a.x - o.x) * (b.z - o.z) - (a.z - o.z) * (b.x - o.x);
	}
}